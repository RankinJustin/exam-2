
// Exam 2
// Justin Rankin

#include <iostream>
#include <conio.h>
#include <fstream>

using namespace std;

float FindAverage(float* pNumbers, int size, float& average)
{
	float Sum = 0;

	for (int i = 0; i < size; i++)
	{

		Sum = Sum + pNumbers[i];
	}

	average = Sum / size;
	return average;

}

int main()
{
	float Average = 0;
	int size = 5;

	float* pUserNumbs = new float[size];
	cout << "Please Enter Five Numbers" << "\n";

	for (int i = 0; i < 5; i++)
	{
		cout << "Number " << (i + 1) << ": ";
		cin >> pUserNumbs[i];
	}

	cout << "Your Numbers Are \n";

	for (int i = 0; i < 5; i++)
	{
		cout << "Number " << i + 1 << " is: " << pUserNumbs[i] << "\n";
	}

	FindAverage(pUserNumbs, size, Average);
	cout << "The average of your nubmers is: " << Average << "\n";

	float max = pUserNumbs[0];
	float min = pUserNumbs[1];

	for (int i = 0; i < size; i++)
	{
		if (pUserNumbs[i] > max)
		{
			max = pUserNumbs[i];
		}
		else if (pUserNumbs[i] < min)
		{
			min = pUserNumbs[i];
		}

	}

	cout << "The max of your numbers is : " << max << "\n";
	cout << "The min of your numbers is : " << min << "\n";

	cout << "Your numbers in reverse order are \n";
	for (int i = size - 1; i > -1; i--)
	{
		cout << "Number " << i + 1 << " is: " << pUserNumbs[i] << "\n";
	}

	char input;
	cout << "Would you like to save to a text file? (y/n)";
	cin >> input;
	if (input == 'y')
	{
		string filepath = "Test.text";
		ofstream ofs(filepath);
		if (ofs)
		{
			ofs << "Your Numbers Are \n";

			for (int i = 0; i < 5; i++)
			{
				ofs << "Number " << i + 1 << " is: " << pUserNumbs[i] << "\n";
			}

			ofs << "The average of your nubmers is: " << Average << "\n";

			ofs << "The max of your numbers is : " << max << "\n";
			ofs << "The min of your numbers is : " << min << "\n";

			ofs << "Your numbers in reverse order are \n";

			for (int i = size - 1; i > -1; i--)
			{
				ofs << "Number " << i + 1 << " is: " << pUserNumbs[i] << "\n";
			}
		}

		ofs.close();
	}




	(void)_getch();
	return 0;
}